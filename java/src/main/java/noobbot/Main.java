package noobbot;

import info.youhavethewrong.slotbot.ActionDecider;
import info.youhavethewrong.slotbot.message.*;
import info.youhavethewrong.slotbot.recv.*;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

import org.slf4j.*;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

public class Main {
    public static void main(String... args) throws IOException {
	String host = args[0];
	int port = Integer.parseInt(args[1]);
	String botName = args[2];
	String botKey = args[3];

	System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

	final Socket socket = new Socket(host, port);
	final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

	final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

	new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    Logger log = LoggerFactory.getLogger(Main.class);

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
	this.writer = writer;
	String line = null;

	send(join);

	ActionDecider imTheDecider = new ActionDecider();

	while ((line = reader.readLine()) != null) {
	    final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
	    if (msgFromServer.msgType.equals("carPositions")) {
		JsonElement data = gson.toJsonTree(msgFromServer.data);
		Type listType = new TypeToken<List<Position>>() {}.getType();
		List<Position> positionList = gson.fromJson(data.getAsJsonArray(), listType);
		assert positionList != null;
		assert positionList.size() > 0;
		CarPositions carPositions = new CarPositions();
		carPositions.setPositions(positionList);
		send(imTheDecider.whatNow(carPositions));
	    } else if (msgFromServer.msgType.equals("join")) {
		send(new Ping());
	    } else if (msgFromServer.msgType.equals("gameInit")) {
		JsonElement data = gson.toJsonTree(msgFromServer.data);
		JsonObject raceData = data.getAsJsonObject().getAsJsonObject("race");
		Type listType = new TypeToken<List<Car>>() {}.getType();
		List<Car> cars = gson.fromJson(raceData.getAsJsonArray("cars"), listType );
		RaceSession raceSession = gson.fromJson(raceData.getAsJsonObject("raceSession"), RaceSession.class);
		Track track = gson.fromJson(raceData.getAsJsonObject("track"), Track.class);
		
		Race race = new Race();
		race.setCars(cars);
		race.setRaceSession(raceSession);
		race.setTrack(track);
		
		GameInit init = new GameInit();
		init.setRace(race);
		imTheDecider.setGame(init);
		imTheDecider.analyzeTrack();
		send(new Ping());
	    } else if (msgFromServer.msgType.equals("gameEnd")) {
		System.out.println("Race end");
		send(new Ping());
	    } else if (msgFromServer.msgType.equals("gameStart")) {
		System.out.println("Race start");
		send(new Ping());
	    } else if (msgFromServer.msgType.equals("lapFinished")) {
		send(new Ping());
	    } else if (msgFromServer.msgType.equals("turboAvailable")) {
		imTheDecider.setTurboAvailable(true);
		send(new Ping());
	    } else {
		send(new Ping());
	    }
	}
    }

    private void send(final SendMsg msg) {
	writer.println(msg.toJson());
	writer.flush();
    }
}
