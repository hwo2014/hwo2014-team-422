package info.youhavethewrong.slotbot.recv;

public class Piece {

    private Double length;
    private Boolean isSwitch;
    private Double radius;
    private Double angle;

    public Double getLength() {
	return length;
    }

    public void setLength(Double length) {
	this.length = length;
    }

    public Boolean getSwitch() {
	return isSwitch;
    }

    public void setSwitch(Boolean isSwitch) {
	this.isSwitch = isSwitch;
    }

    public Double getRadius() {
	return radius;
    }

    public void setRadius(Double radius) {
	this.radius = radius;
    }

    public Double getAngle() {
	return angle;
    }

    public void setAngle(Double angle) {
	this.angle = angle;
    }
}
