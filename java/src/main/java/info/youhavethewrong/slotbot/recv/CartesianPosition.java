package info.youhavethewrong.slotbot.recv;

public class CartesianPosition {

    private double x;
    private double y;

    public double getX() {
	return x;
    }

    public void setX(double x) {
	this.x = x;
    }

    public double getY() {
	return y;
    }

    public void setY(double y) {
	this.y = y;
    }
}
