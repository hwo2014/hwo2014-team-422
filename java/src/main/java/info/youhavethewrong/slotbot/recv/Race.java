package info.youhavethewrong.slotbot.recv;

import java.util.List;

public class Race {

    private Track track;
    private RaceSession raceSession;
    private List<Car> cars;

    public Track getTrack() {
	return track;
    }

    public void setTrack(Track track) {
	this.track = track;
    }

    public RaceSession getRaceSession() {
	return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
	this.raceSession = raceSession;
    }

    public List<Car> getCars() {
	return cars;
    }

    public void setCars(List<Car> cars) {
	this.cars = cars;
    }
}
