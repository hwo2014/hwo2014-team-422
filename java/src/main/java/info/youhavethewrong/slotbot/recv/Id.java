package info.youhavethewrong.slotbot.recv;

public class Id {

    private Object name;
    private String color;

    public Object getName() {
	return name;
    }

    public void setName(Object name) {
	this.name = name;
    }

    public String getColor() {
	return color;
    }

    public void setColor(String color) {
	this.color = color;
    }
}
