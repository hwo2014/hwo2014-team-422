package info.youhavethewrong.slotbot.recv;

public class Lane {

    private double startLaneIndex;
    private double endLaneIndex;

    public double getEndLaneIndex() {
	return endLaneIndex;
    }

    public void setEndLaneIndex(double endLaneIndex) {
	this.endLaneIndex = endLaneIndex;
    }

    public double getStartLaneIndex() {
	return startLaneIndex;
    }

    public void setStartLaneIndex(double startLaneIndex) {
	this.startLaneIndex = startLaneIndex;
    }
}
