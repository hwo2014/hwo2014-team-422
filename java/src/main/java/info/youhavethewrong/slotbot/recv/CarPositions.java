package info.youhavethewrong.slotbot.recv;

import java.util.List;

public class CarPositions {

    private List<Position> positions;

    public List<Position> getPositions() {
	return positions;
    }

    public void setPositions(List<Position> positions) {
	this.positions = positions;
    }
}
