package info.youhavethewrong.slotbot.recv;

public class PiecePosition {

    private int pieceIndex;
    private double inPieceDistance;
    private Lane lane;
    private double lap;

    public int getPieceIndex() {
	return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
	this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
	return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
	this.inPieceDistance = inPieceDistance;
    }

    public Lane getLane() {
	return lane;
    }

    public void setLane(Lane lane) {
	this.lane = lane;
    }

    public double getLap() {
	return lap;
    }

    public void setLap(double lap) {
	this.lap = lap;
    }
}
