package info.youhavethewrong.slotbot.recv;

public class GameInit {

    private Race race;

    public Race getRace() {
	return race;
    }

    public void setRace(Race race) {
	this.race = race;
    }
}
