package info.youhavethewrong.slotbot.recv;

public class StartingPoint {

    private CartesianPosition position;
    private Double angle;

    public CartesianPosition getPosition() {
	return position;
    }

    public void setPosition(CartesianPosition position) {
	this.position = position;
    }

    public Double getAngle() {
	return angle;
    }

    public void setAngle(Double angle) {
	this.angle = angle;
    }
}
