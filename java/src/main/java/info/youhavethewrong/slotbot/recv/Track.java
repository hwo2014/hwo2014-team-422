package info.youhavethewrong.slotbot.recv;

import java.util.List;

public class Track {

    private String id;
    private String name;
    private List<Object> pieces;
    private List<Lane> lanes;
    private StartingPoint startingPoint;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<Object> getPieces() {
	return pieces;
    }

    public void setPieces(List<Object> pieces) {
	this.pieces = pieces;
    }

    public List<Lane> getLanes() {
	return lanes;
    }

    public void setLanes(List<Lane> lanes) {
	this.lanes = lanes;
    }

    public StartingPoint getStartingPoint() {
	return startingPoint;
    }

    public void setStartingPoint(StartingPoint startingPoint) {
	this.startingPoint = startingPoint;
    }
}
