package info.youhavethewrong.slotbot.recv;

public class Position {

    private Id id;
    private double angle;
    private PiecePosition piecePosition;

    public Id getId() {
	return id;
    }

    public void setId(Id id) {
	this.id = id;
    }

    public double getAngle() {
	return angle;
    }

    public void setAngle(double angle) {
	this.angle = angle;
    }

    public PiecePosition getPosition() {
	return piecePosition;
    }

    public void setPosition(PiecePosition position) {
	this.piecePosition = position;
    }
}
