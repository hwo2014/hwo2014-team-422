package info.youhavethewrong.slotbot.message;

public class Turbo extends SendMsg {

    @Override
    protected String msgType() {
	return "turbo";
    }

    @Override
    protected Object msgData() {
	return "Go speed racer, go!";
    }
}
