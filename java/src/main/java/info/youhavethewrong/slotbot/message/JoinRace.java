package info.youhavethewrong.slotbot.message;

public class JoinRace extends SendMsg {

    public JoinData joinData;

    public JoinRace(String name, String key, String mapName, int carCount) {
	joinData = new JoinData();
	joinData.trackName = mapName;
	joinData.carCount = carCount;
	joinData.botId = new BotId();
	joinData.botId.key = key;
	joinData.botId.name = name;
    }

    @Override
    protected String msgType() {
	return "joinRace";
    }

    @Override
    protected Object msgData() {
	return this.joinData;
    }

    public class BotId {
	public String name;
	public String key;
    }

    public class JoinData {
	public BotId botId;
	public String trackName;
	public int carCount;
    }
}
