package info.youhavethewrong.slotbot.message;

public class Ping extends SendMsg {
    @Override
    protected String msgType() {
	return "ping";
    }
}