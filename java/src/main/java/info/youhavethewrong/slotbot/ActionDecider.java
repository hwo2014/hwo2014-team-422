package info.youhavethewrong.slotbot;

import java.util.*;

import org.slf4j.*;

import com.google.gson.*;

import info.youhavethewrong.slotbot.message.*;
import info.youhavethewrong.slotbot.recv.*;

public class ActionDecider {

    private static final double HARD_DECEL = 1.0d;
    private static final double HARD_ACCEL = 0.2d;
    private static final double DECEL = 0.1d;
    private static final double ACCEL = 0.1d;
    private static final String MYKEY = "lwgd328hHd2IlQ";
    private static final String MYNAME = "SlotBot2000";
    private static final String MYTEAM = "ChubbyBadger";

    private double minS = 3.0d;

    private GameInit game;
    private Position leaderPosition;
    private double throttle = 0.5d;
    private double lastAngle = 0.0d;
    private double lastDistance = 0.0d;
    private boolean switched = false;
    private int lastPieceIndex = 0;
    private int switchedPieceIndex = 0;
    private boolean turboAvailable = false;

    public SendMsg whatNow(CarPositions positions) {
	Position myPos = findMe(positions.getPositions());
	if (goTime(myPos) && turboAvailable) {
	    turboAvailable = false;
	    return new Turbo();
	}
	int currentPieceIndex = myPos.getPosition().getPieceIndex();
	int trackPieces = game.getRace().getTrack().getPieces().size();

	Piece lastTrackPiece = parseTrackPiece(game.getRace().getTrack().getPieces().get(lastPieceIndex));
	Piece currentTrackPiece = parseTrackPiece(game.getRace().getTrack().getPieces().get(currentPieceIndex));
	double deltaS = 0.0d;
	if (currentPieceIndex == lastPieceIndex) {
	    deltaS = myPos.getPosition().getInPieceDistance() - lastDistance;
	} else {
	    if (lastTrackPiece.getLength() != null) {
		deltaS = (lastTrackPiece.getLength() - lastDistance) + myPos.getPosition().getInPieceDistance();
	    } else {
		deltaS = (lastTrackPiece.getRadius() - lastDistance) + myPos.getPosition().getInPieceDistance();
	    }
	}

	Piece nextTrackPiece = parseTrackPiece(game.getRace().getTrack().getPieces().get((currentPieceIndex + 1) % trackPieces));
	Piece afterNextTrackPiece = parseTrackPiece(game.getRace().getTrack().getPieces().get((currentPieceIndex + 2) % trackPieces));

	Double length = currentTrackPiece.getLength();
	Double angle = currentTrackPiece.getAngle();

	if (length != null) {
	    if (deltaS < minS) {
		accelerate(ACCEL);
	    } else if (((currentTrackPiece.getLength() - myPos.getPosition().getInPieceDistance() > (currentTrackPiece.getLength() / 2.0)) || currentTrackPiece
		    .getLength() < 100.0d) && shouldCoast(myPos, currentTrackPiece, nextTrackPiece, afterNextTrackPiece)) {
		decelerate(HARD_DECEL);
	    } else if (Math.abs(myPos.getAngle()) > Math.abs(lastAngle) + 5) {
		decelerate(DECEL);
	    } else if (throttle < 0.5d) {
		throttle = 0.5d;
	    } else {
		accelerate(HARD_ACCEL);
	    }
	} else if (angle != null) {
	    if (deltaS < minS) {
		accelerate(ACCEL);
	    } else if (shouldCoast(myPos, currentTrackPiece, nextTrackPiece, afterNextTrackPiece)) {
		decelerate(HARD_DECEL);
	    } else if (Math.abs(myPos.getAngle()) > 32.0d) {
		decelerate(HARD_DECEL);
	    }
	}

	if (currentTrackPiece.getSwitch() != null && currentTrackPiece.getSwitch()) {
	    if (switchedPieceIndex != currentPieceIndex && !switched) {
		if (nextTrackPiece.getAngle() != null) {
		    String randomSwitch = "";
		    if (nextTrackPiece.getRadius() / Math.abs(nextTrackPiece.getAngle()) > 2) {
			randomSwitch = (nextTrackPiece.getAngle() > 0) ? "Left" : "Right";
		    } else {
			randomSwitch = (nextTrackPiece.getAngle() < 0) ? "Left" : "Right";
		    }
		    switchedPieceIndex = currentPieceIndex;
		    switched = true;
		    return new SwitchLane(randomSwitch);
		} else if (afterNextTrackPiece.getAngle() != null) {
		    String randomSwitch = "";
		    if (afterNextTrackPiece.getRadius() / Math.abs(afterNextTrackPiece.getAngle()) > 2) {
			randomSwitch = (afterNextTrackPiece.getAngle() > 0) ? "Left" : "Right";
		    } else {
			randomSwitch = (afterNextTrackPiece.getAngle() < 0) ? "Left" : "Right";
		    }
		    switchedPieceIndex = currentPieceIndex;
		    switched = true;
		    return new SwitchLane(randomSwitch);
		}
	    }
	} else {
	    switched = false;
	}

	lastAngle = myPos.getAngle();
	lastPieceIndex = currentPieceIndex;
	lastDistance = myPos.getPosition().getInPieceDistance();
	return new Throttle(throttle);
    }

    public GameInit getGame() {
	return game;
    }

    public void setGame(GameInit game) {
	this.game = game;
    }

    boolean shouldAccelerate(Position myPos, double arcLength) {
	return (arcLength - myPos.getPosition().getInPieceDistance()) < (arcLength / 3.0d);
    }

    boolean shouldFloorIt(Position myPos, double arcLength) {
	return (arcLength - myPos.getPosition().getInPieceDistance()) < (arcLength / 4.0d);
    }

    boolean shouldCoast(Position myPos, Piece currentTrackPiece, Piece nextTrackPiece, Piece afterNextTrackPiece) {
	return (nextTrackPiece.getAngle() != null && (Math.abs(nextTrackPiece.getAngle()) > 40.0d || nextTrackPiece.getRadius() < 51.0d))
		|| (afterNextTrackPiece.getAngle() != null && (Math.abs(afterNextTrackPiece.getAngle()) > 40.0d || afterNextTrackPiece
			.getRadius() < 51.0d))
		|| (nextTrackPiece.getLength() != null && nextTrackPiece.getLength() < 51.0d && (afterNextTrackPiece.getAngle() != null && afterNextTrackPiece
			.getRadius() < 100.0d));
    }

    void accelerate(double increment) {
	if ((throttle + increment) > 1.0d) {
	    throttle = 1.0d;
	} else {
	    throttle += increment;
	}
    }

    void decelerate(double decrement) {
	if ((throttle - decrement) < 0.0d) {
	    throttle = 0.0d;
	} else {
	    throttle -= decrement;
	}
    }

    private Piece parseTrackPiece(Object jsonObject) {
	Piece piece = new Piece();
	JsonObject jsonPiece = new JsonParser().parse(jsonObject.toString()).getAsJsonObject();
	if (jsonPiece.has("length")) {
	    piece.setLength(jsonPiece.get("length").getAsDouble());
	}
	if (jsonPiece.has("switch")) {
	    piece.setSwitch(jsonPiece.get("switch").getAsBoolean());
	}
	if (jsonPiece.has("angle")) {
	    piece.setAngle(jsonPiece.get("angle").getAsDouble());
	}
	if (jsonPiece.has("radius")) {
	    piece.setRadius(jsonPiece.get("radius").getAsDouble());
	}
	return piece;
    }

    private Position findMe(List<Position> positions) {
        Position myPosition = null;
	for (Position position : positions) {
	    String name = position.getId().getName().toString();
	    if (name.equalsIgnoreCase(MYNAME) || name.equalsIgnoreCase(MYKEY) || name.equalsIgnoreCase(MYTEAM)) {
                myPosition = position;
	    }
	}
        return myPosition;
    }

    public void analyzeTrack() {
	List<Map<Integer, Piece>> angleSections = new LinkedList<Map<Integer, Piece>>();
	Map<Integer, Piece> angleSection = new LinkedHashMap<Integer, Piece>();
	List<Object> pieces = game.getRace().getTrack().getPieces();
	for (int i = 0; i < pieces.size(); i++) {
	    Piece trackPiece = parseTrackPiece(pieces.get(i));
	    if (trackPiece.getAngle() != null) {
		angleSection.put(i, trackPiece);
	    } else {
		if (angleSection.size() > 0) {
		    angleSections.add(angleSection);
		    angleSection = new LinkedHashMap<Integer, Piece>();
		}
	    }
	}

	double maxAngles = 0.0d;
	double maxRadius = 0.1d;

	for (int i = 0; i < angleSections.size(); i++) {
	    double tempAngles = 0.0d;
	    double tempRadius = 0.0d;
	    for (Integer index : angleSections.get(i).keySet()) {
		tempAngles += Math.abs(angleSections.get(i).get(index).getAngle());
		tempRadius += angleSections.get(i).get(index).getRadius();
	    }
	    if (tempAngles / tempRadius > maxAngles / maxRadius) {
		maxAngles = tempAngles;
		maxRadius = tempRadius;
	    }
	}

	setMinDisplacementFromAnalysis((maxAngles / maxRadius));
    }

    private void setMinDisplacementFromAnalysis(double coefficient) {
	minS = 9.71779 * Math.pow(Math.E, (-0.764664 * coefficient));
    }

    public void setTurboAvailable(boolean turboAvailable) {
	this.turboAvailable = turboAvailable;
    }

    private boolean goTime(Position position) {
	return position.getPosition().getLap() == (game.getRace().getRaceSession().getLaps() - 1.0)
		&& straightOnTilMorning(position.getPosition().getPieceIndex());
    }

    private boolean straightOnTilMorning(int pieceIndex) {
	List<Object> trackPieces = game.getRace().getTrack().getPieces();
	for (int i = pieceIndex; i < trackPieces.size(); i++) {
	    if (parseTrackPiece(trackPieces.get(i)).getLength() == null) {
		return false;
	    }
	}
	return true;
    }
}
